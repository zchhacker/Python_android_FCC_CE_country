# Python_android_FCC_CE_country

#### 介绍
Python 3.9 脚本
获取FCC、CE国家和信道
配置国家AX功率

#### 软件架构
软件架构说明


#### 安装教程

1.  安装Python 3.9
2.  git clone 仓库
3.  运行Get_All_Country_Channels_Test.bat，检查是否运行成功
4.  第3步运行成功，运行get_country_content.py
5.  结果输出在des_struct.txt

#### 使用说明

1.  文件说明  
1）FCC_CE_Country目录下的FCC.txt、CE.txt文件记录了对应的国家  
2）修改Code目录下的结构体，将3*6个RU参数改为需要设定的power值，国家和信道不需要修改  
2.  连上设备，确保cmd有adb指令，代码中会执行以下3条命令来获取国家信道  
① adb shell cmd wifi force-country-code enabled CN  
② adb shell wpa_cli -iwlan0 get_capability channels  
③ adb shell cmd wifi force-country-code disabled  
3.  运行Get_All_Country_Channels_Test.bat，测试adb是否可用，会查找所有FCC、CE国家的信道，结果输出在FCC_CE_Country目录  
① CE_Country_Channels.txt  
② FCC_Country_Channels.txt  
4.  第3步确认可以执行成功，运行get_country_content.py  
1）会依次对FCC_CE_Country目录下FCC.txt、CE.txt文件内国家进行遍历查询信道  
2）根据国家和信道匹配Code目录下的结构体，记录到des_struct.txt文件中  
5.  最终结果输出在des_struct.txt文件中

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
