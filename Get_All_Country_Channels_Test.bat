@echo off
adb wait-for-device
adb root

:: Country Channels File
set FCC_Country_Channels_File="FCC_CE_Country\FCC_Country_Channels.txt"
set CE_Country_Channels_File="FCC_CE_Country\CE_Country_Channels.txt"

:: clean files
type nul > %FCC_Country_Channels_File%
type nul > %CE_Country_Channels_File%

:: Get FCC Country Channels
for /f "tokens=*" %%c in (FCC_CE_Country\FCC.txt) do (
	echo country is %%c
    echo country is %%c >> %FCC_Country_Channels_File%
	adb shell cmd wifi force-country-code enabled %%c
	adb shell wpa_cli -iwlan0 get_capability channels >> %FCC_Country_Channels_File%
	adb shell cmd wifi force-country-code disabled
)

for /f "tokens=*" %%c in (FCC_CE_Country\CE.txt) do (
	echo country is %%c
    echo country is %%c >> %CE_Country_Channels_File%
	adb shell cmd wifi force-country-code enabled %%c
	adb shell wpa_cli -iwlan0 get_capability channels >> %CE_Country_Channels_File%
	adb shell cmd wifi force-country-code disabled
)

pause